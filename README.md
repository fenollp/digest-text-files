# Finding structure in chaos - Finding similarities among the unfamiliar - Separating style and content

  The aim of those ideas is to convert any web page written in (x)HTML +
Javascript + CSS into two separate files : XML + XSLT.  
Typically, the XSLT file describes the stylish part of the former web page,
which multiple other web page may share. And the XML one would hold within
its content : what makes some two web pages of same shape different.


Here are some ideas of how to realise this project :
====================================================
They may be sorted from the most naïve one to the most clever one.

  The most common scenario describes two web pages A and B of same style and
structure — understand shape. They only differ from their very content. We
splitthose two files into two XML files sharing the same tag-tree and a third
containing Transformation information of this tree.  
Keep in mind that we are manipulating XML-ish files : they all've got some
structure already.  
We must be able to rebuild the former web pages from upon the built XML files!

diff:
-----
Unregarding to the structure that an HTML (or XML-derivate) page has,
we use the UNIX diff() command to compare A & B.  
Here is what's done with the GNU diff(1) :

First the command:

    $ diff --ignore-case --ignore-file-name-case --ignore-tab-expansion      \
    --ignore-space-change --ignore-all-space --ignore-blank-lines --brief    \
    --normal --side-by-side --suppress-common-lines --paginate --expand-tabs \
    --report-identical-files --from-file=A --to-file=B --minimal             \
    --speed-large-files

And now the result:


Emptying:
---------
regex: replace '<[^<>]+>' with '' and then do the opposite .?


Massively Concurrent Bruteforce
-


Neural Network
-



A meaning problem:
==================
	[1,3..] What should that be translated to?
A geometric or arithmetic series? or what then? Maybe I should ask 1000 person?
	This is I think a problem that this project will eventually encouter…



Real World Applications
=======================

Compression
-----------
The idea is to have a table of blocks of bits of different sizes which blocks
can be found inside the file. A new encoding is set (on an optimum number of
bits) indexing this table, then the file is rewritten in this encoding.


Computer Code Optimization
--------------------------
Dig through a wide bunch of Open Source assembler code (just `gcc -c things`).
By 'digesting' this text, the most common patterns (series of instructions,
behaviors) will come up. Common patterns of more than one ASM instruction could
then be implemented in only one instruction. The idea is that hardware
implementation is faster… you know…

Caching Web Resources
---------------------
Extract the form from the function from generated HTML pages.
Serve the form once and the function often. See XSLT/XML or XSLT/JS/JSON here.

Natural Language
----------------
Find out which group of words come up most often among a collection of written
works in a given spoken language. May help to associate it with their audio
counterpart. Surely would help finding most common words gatherings,
such as idioms, or contemporary idioms.

Hypotheticaly…
--------------
Actually, if it were given text in a language that can describe anything (but
syntactically/semantically in only one way) it would extract meaning!
